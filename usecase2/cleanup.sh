#!/bin/bash

cleanup/cleanup_jwt.sh
cleanup/cleanup_rhsso.sh
cleanup/cleanup_bookinfo.sh

if [ -f demo_notes ] ; then
    rm -f demo_notes
fi
