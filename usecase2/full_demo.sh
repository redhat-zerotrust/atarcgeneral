#!/bin/bash
function check_pod(){
    PROJECT=$1
    NUM=$2
    CONDITION=$3
    COUNT=0
    while [ ${COUNT} -lt ${NUM} ];
    do
        clear
        oc get pods -n ${PROJECT}
        sleep 5
        COUNT=$( oc get pods -n ${PROJECT} --field-selector=status.phase=Running --no-headers|wc -l)
    done
}

function backup_istio_files(){
    for file in $(ls istio/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_istio_files(){
    for file in $(ls istio/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f istio/*.bak
}

function backup_bookinfo_files(){
    for file in $(ls bookinfo/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_bookinfo_files(){
    for file in $(ls bookinfo/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f bookinfo/*.bak
}

function get_control_plane_status(){
  DONE=1
  while [ $DONE -ne 0 ];
  do
    clear
    CURRENT_STATUS=$(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.annotations.readyComponentCount}')
    printf "Ready Component Count: %s\n" "$CURRENT_STATUS"
    READY=$(echo $CURRENT_STATUS|awk -F'/' '{print $1}')
    TOTAL=$(echo $CURRENT_STATUS|awk -F'/' '{print $2}')
    if [ $READY -gt 0 ];
    then
      printf "Ready: \n"
      for i in $(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.readiness.components.ready[*]}')
      do
        printf "=> %s\n" "$i"
      done
    fi
    if [ $READY -eq  $TOTAL ];
    then
      DONE=0
    fi
    sleep 20
  done
}

function verify_sidecar(){
  PROJECT=$1
  for pod in $(oc get pods -n $PROJECT --no-headers -o=custom-columns='DATA:metadata.name')
  do
    NUM=$(oc get pod $pod -n $PROJECT -o jsonpath='{.spec.containers[*].name}' | wc -w)
    if [ $NUM -lt 2 ];
    then
      echo "Sidecar not found for pod $pod"
      oc delete pod $pod -n $PROJECT
    else
      echo "pod $pod already has 2 containers"
    fi
  done
}


function backup_rhsso_files(){
    for file in $(ls rhsso/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_rhsso_files(){
    for file in $(ls rhsso/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f rhsso/*.bak
}

function backup_oauth_files(){
    for file in $(ls oauth2-proxy/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_oauth_files(){
    for file in $(ls oauth2-proxy/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f oauth2-proxy/*.bak
}

function backup_jwt_files(){
    for file in $(ls jwt/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_jwt_files(){
    for file in $(ls jwt/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f jwt/*.bak
}

PLATFORM=$(uname)

if ! hash oc 2>/dev/null
then
    echo "'oc' was not found in PATH"
    echo "Download from https://mirror.openshift.com/pub/openshift-v4/clients/oc/"
    exit
fi

if ! hash oc whoami 2>/dev/null
then
    echo "You need to login to your cluster with oc login --server=<URL to API>"
    exit 
fi

echo "Enter the name of Service Mesh Control Plane project: "
read CONTROL_PLANE
echo

oc get project $CONTROL_PLANE > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $CONTROL_PLANE already exists"
  sleep 2
else
  echo "Creating project: $CONTROL_PLANE"
  oc new-project $CONTROL_PLANE --display-name $CONTROL_PLANE --description="Service Mesh Control Plane" 1>/dev/null
fi

echo "Enter the name of Bookinfo project: "
read BOOKINFO
echo

oc get project $BOOKINFO > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $BOOKINFO already exists"
  sleep 2
else
  echo "Creating project: $BOOKINFO"
  oc new-project $BOOKINFO --display-name $BOOKINFO --description="Bookinfo Demo" 1>/dev/null
fi

echo "Enter the name of RHSSO project: "
read RHSSO
echo

SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Route will under domain *.${DOMAIN}"

echo "Do you want to change domain? (y/n): "
read CHANGE_DOMAIN
echo

if [ "$CHANGE_DOMAIN" = "Y" ] || [ "$CHANGE_DOMAIN" = "y" ];
then
    echo "Enter new domain name: "
    read DOMAIN
fi

echo "Check Control Plane"
get_control_plane_status

echo "Join Bookinfo to Control Plane"
echo "Add - bookinfo to spec: to save time"
echo "press enter to continue:"
read

oc -n $CONTROL_PLANE edit smmr

echo 
oc describe smmr/default -n $CONTROL_PLANE | grep -A3 Spec:
sleep 5

backup_bookinfo_files

echo "Deploy applications to $BOOKINFO"
oc apply -f bookinfo/01_bookinfo.yaml -n $BOOKINFO

echo "Create Gateway and VirtualService"
oc apply -f bookinfo/02_bookinfo-gateway.yaml -n $BOOKINFO

check_pod $BOOKINFO 6 Running

restore_bookinfo_files

backup_istio_files

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/BOOKINFO/'"$BOOKINFO"'/' istio/*.yaml
  sed -i '' 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' istio/*.yaml
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' istio/*.yaml
else
  sed -i 's/BOOKINFO/'"$BOOKINFO"'/' istio/*.yaml
  sed -i 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' istio/*.yaml
  sed -i 's/DOMAIN/'"$DOMAIN"'/' istio/*.yaml
fi

echo 
echo "Apply istio configuration for mutual TLS authentication to applications"
oc apply -f istio/03_destination-rule-all-mtls.yaml -n $BOOKINFO
oc apply -f istio/04_details-peer-authentication.yaml -n $BOOKINFO
oc apply -f istio/05_productpage-peer-authentication.yaml -n $BOOKINFO
oc apply -f istio/06_ratings-peer-authentication.yaml -n $BOOKINFO
oc apply -f istio/07_reviews-peer-authentication.yaml -n $BOOKINFO
echo 

restore_istio_files

echo
echo "Check pods in $BOOKINFO"
verify_sidecar $BOOKINFO

GATEWAY_URL=$(oc -n $CONTROL_PLANE get route istio-ingressgateway -o jsonpath='{.spec.host}')

echo "Check http://$GATEWAY_URL/productpage:"
echo
curl -I http://$GATEWAY_URL/productpage
echo
echo "Check https://$GATEWAY_URL/productpage:"
echo
curl -I https://$GATEWAY_URL/productpage
echo "Contact host to validate service in incognito window:"
echo "https://$GATEWAY_URL/productpage"

echo "Retaining URL in demo_notes..."
cat << EOF >> demo_notes
Contact host to validate service in incognito window:
https://$GATEWAY_URL/productpage

EOF


SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Redirect Route will under domain *.${DOMAIN}"


PLATFORM=$(uname)

if ! hash oc 2>/dev/null
then
    echo "'oc' was not found in PATH"
    echo "Download from https://mirror.openshift.com/pub/openshift-v4/clients/oc/"
    exit
fi

if ! hash oc whoami 2>/dev/null
then
    echo "You need to login to your cluster with oc login --server=<URL to API>"
    exit
fi

backup_rhsso_files

oc get project $RHSSO > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $RHSSO exists, continuing..."
  sleep 2
else
  echo "Create project: $RHSSO and install RHSSO Operator"
  exit 1
fi

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' rhsso/*.yaml
else
  sed -i 's/DOMAIN/'"$DOMAIN"'/' rhsso/*.yaml
fi

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/RHSSO/'"$RHSSO"'/' rhsso/*.yaml
else
  sed -i 's/RHSSO/'"$RHSSO"'/' rhsso/*.yaml
fi

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' rhsso/*.yaml
else
  sed -i 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' rhsso/*.yaml
fi


echo "Create the RHSSO deployment as follows:"
cat rhsso/01_rhsso.yaml
echo
oc apply -f rhsso/01_rhsso.yaml -n $RHSSO

echo 
echo "Wait for rhsso startup..."
check_pod $RHSSO 3 Running
echo

echo "Create the RHSSO servicemesh-lab realm:"
cat rhsso/02_servicemesh-realm.yaml
echo
oc apply -f rhsso/02_servicemesh-realm.yaml -n $RHSSO

echo
echo "Create a client istio inside the realm servicemesh-lab."
echo "Note: the field secret is set randomly and you can leave as it is (we will use "
echo "this secret later); even if the route matching the redirectUris is currently"
echo "using http (the route of the Istio default ingress gateway), leave https in "
echo "the redirectUris field (the route will be patched later)"
cat rhsso/03_istio-client.yaml
echo
oc apply -f rhsso/03_istio-client.yaml -n $RHSSO

echo
echo "Create the local user localuser:localuser inside the realm servicemesh-lab"
cat rhsso/04_local-user.yaml
echo
oc apply -f rhsso/04_local-user.yaml -n $RHSSO

restore_rhsso_files


RHSSO_ROUTE=$(oc get route keycloak -n $RHSSO | grep keycloak | awk '{print $2}')
RHSSO_ADMIN_CREDENTIAL=$(oc get secret credential-rhsso-simple --template={{.data.ADMIN_PASSWORD}} -n $RHSSO | base64 --decode)

echo
echo "Log into https://${RHSSO_ROUTE}/auth as admin with credential ${RHSSO_ADMIN_CREDENTIAL} and check the following:"
echo
echo " - ensure Standard Flow Enabled is enabled for this client; Clients>Istio>Settings>Line 10"
echo " - ensure that the Access Type value is set to confidential; if not, set it Clients>Istio>Settings>Line 9"
echo " - click on the button Save at the bottom of the page; a new "Credentials" tab has appeared at the top of the page,"
echo "and you can check in this tab that the client secret is matching the value set in 03_istio-client.yaml (if not, simply use the new value)."
echo " - On Client Scopes tab, verify Assigned Default Client Scopes contains email and profile"
echo
echo "You can test to login as localuser:localuser by opening https://$RHSSO_ROUTE/auth/realms/servicemesh-lab/account in a browser."
echo
echo "Retaining URLs in demo_notes..."

cat << EOF >> demo_notes

https://${RHSSO_ROUTE}/auth credential ${RHSSO_ADMIN_CREDENTIAL}
https://$RHSSO_ROUTE/auth/realms/servicemesh-lab/account

EOF

backup_oauth_files

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/RHSSO/'"$RHSSO"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
else
  sed -i 's/RHSSO/'"$RHSSO"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
  sed -i 's/DOMAIN/'"$DOMAIN"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
fi


echo "Add the oauth2-proxy container to the Istio Ingress Gateway service:"
cat oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
echo
oc patch deploy istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml)"
echo

sleep 40
get_control_plane_status

echo "Add the oauth2-proxy port to the Istio Ingress Gateway service:"
cat oauth2-proxy/02_patch-istio-ingressgateway-svc.yaml
echo
oc patch svc istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/02_patch-istio-ingressgateway-svc.yaml)"
echo

echo "Set edge TLS termination for the Istio Ingress Gateway route:"
cat oauth2-proxy/03_patch-istio-ingressgateway-route-edge.yaml
echo
oc patch route istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/03_patch-istio-ingressgateway-route-edge.yaml)"
echo

echo "Make the Istio Ingress Gateway route target the oauth2-proxy container:"
cat oauth2-proxy/04_patch-istio-ingressgateway-route-oauth.yaml
echo
oc patch route istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/04_patch-istio-ingressgateway-route-oauth.yaml)" 
echo

restore_oauth_files

echo "Retrieve the CA certificate from secret in openshift-ingress-operator project"
oc extract secret/router-ca -n openshift-ingress-operator --to=/tmp/ --confirm

echo "Create a secret from the CA certificate in control-plane project"
oc create secret generic openshift-wildcard --from-file=extra.pem=/tmp/tls.crt -n $CONTROL_PLANE

echo "Mount the CA secret at the specific location /cacerts/extra.pem in istiod pod"
oc set volumes -n $CONTROL_PLANE deployment/istiod-basic-install --add  --name=extracacerts  --mount-path=/cacerts  --secret-name=openshift-wildcard  --containers=discovery

echo "Cleaning up extracted certs"
rm -f /tmp/tls.key /tmp/tls.cert

RHSSO_ROUTE=$(oc get route keycloak -n $RHSSO | grep rhsso | awk '{print $2}')

echo "Validate certificate install.. perform the following in the shell prompt:"

cat << EOF
$ oc project control-plane

$ oc get pod
[...]
istiod-basic-6f5bfd89bf-v49vf          1/1     Running   0          14s
[...]

# RSH to istiod-basic pod
$ oc rsh istiod-basic-6f5bfd89bf-v49vf 

# Check connection to RHSSO without the CA
[pod] sh-4.4$ curl -I https://$RHSSO_ROUTE/auth/
curl: (60) SSL certificate problem: self signed certificate in certificate chain

# Check connection to RHSSO with the CA
[pod] sh-4.4$ curl --cacert /cacerts/extra.pem -I https://$RHSSO_ROUTE/auth/
HTTP/1.1 200 OK
EOF


backup_jwt_files

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/RHSSO/'"$RHSSO"'/' jwt/*.yaml
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' jwt/*.yaml
  sed -i '' 's/BOOKINFO/'"$BOOKINFO"'/' jwt/*.yaml
else
  sed -i 's/RHSSO/'"$RHSSO"'/' jwt/*.yaml
  sed -i 's/DOMAIN/'"$DOMAIN"'/' jwt/*.yaml
  sed -i 's/BOOKINFO/'"$BOOKINFO"'/' jwt/*.yaml
fi

echo "Set up request authentication policies:"
cat jwt/01_requestauthentication.yaml
echo
oc apply -f jwt/01_requestauthentication.yaml -n $BOOKINFO
echo

echo "Set up authorization policies:"
cat jwt/02_authpolicy_allow_from_servicemesh-lab_realm.yaml
echo
oc apply -f jwt/02_authpolicy_allow_from_servicemesh-lab_realm.yaml -n $BOOKINFO

restore_jwt_files

echo "Command line portions of the lab setup are complete"
echo
echo "Please verify settings and continue with setup."

cat << EOF >> demo_notes

# Revoke authorization:
oc edit authorizationpolicy authpolicy -n bookinfo

# Grab all activity by user:
oc -n control-plane logs istio-ingressgateway-{pod} oauth2-proxy | grep ${userid}

# Confirm access denied:
oc -n rhsso logs keycloak-0 | grep WARN

EOF


if [ -f demo_notes ] ; then
    cat demo_notes
fi


