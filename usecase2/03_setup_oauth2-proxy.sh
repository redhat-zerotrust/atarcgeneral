#!/bin/bash

function check_pod(){
    PROJECT=$1
    NUM=$2
    CONDITION=$3
    COUNT=0
    while [ ${COUNT} -lt ${NUM} ];
    do
        clear
        oc get pods -n ${PROJECT}
        sleep 5
        COUNT=$( oc get pods -n ${PROJECT} --field-selector=status.phase=Running --no-headers|wc -l)
    done
}

function get_control_plane_status(){
  DONE=1
  while [ $DONE -ne 0 ];
  do
    clear
    CURRENT_STATUS=$(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.annotations.readyComponentCount}')
    printf "Ready Component Count: %s\n" "$CURRENT_STATUS"
    READY=$(echo $CURRENT_STATUS|awk -F'/' '{print $1}')
    TOTAL=$(echo $CURRENT_STATUS|awk -F'/' '{print $2}')
    if [ $READY -gt 0 ];
    then
      printf "Ready: \n"
      for i in $(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.readiness.components.ready[*]}')
      do
        printf "=> %s\n" "$i"
      done
    fi
    if [ $READY -eq  $TOTAL ];
    then
      DONE=0
    fi
    sleep 20
  done
}

function backup_oauth_files(){
    for file in $(ls oauth2-proxy/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_oauth_files(){
    for file in $(ls oauth2-proxy/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f oauth2-proxy/*.bak
}

SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Route will under domain *.${DOMAIN}"

echo "Do you want to change domain? (y/n): "
read CHANGE_DOMAIN
echo

if [ "$CHANGE_DOMAIN" = "Y" ] || [ "$CHANGE_DOMAIN" = "y" ];
then
    echo "Enter new domain name: "
    read DOMAIN
fi

echo "Enter the name of Service Mesh Control Plane project: "
read CONTROL_PLANE
echo

echo "Enter the name of RHSSO project: "
read RHSSO
echo

backup_oauth_files

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/RHSSO/'"$RHSSO"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
else
  sed -i 's/RHSSO/'"$RHSSO"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
  sed -i 's/DOMAIN/'"$DOMAIN"'/' oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
fi


echo "Add the oauth2-proxy container to the Istio Ingress Gateway service:"
cat oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml
echo
echo "Press enter to continue:"
read
oc patch deploy istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/01_patch-istio-ingressgateway-deploy.yaml)"
echo

sleep 40
get_control_plane_status

echo "Add the oauth2-proxy port to the Istio Ingress Gateway service:"
cat oauth2-proxy/02_patch-istio-ingressgateway-svc.yaml
echo
echo "Press enter to continue:"
read
oc patch svc istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/02_patch-istio-ingressgateway-svc.yaml)"
echo

echo "Set edge TLS termination for the Istio Ingress Gateway route:"
cat oauth2-proxy/03_patch-istio-ingressgateway-route-edge.yaml
echo
echo "Press enter to continue:"
read
oc patch route istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/03_patch-istio-ingressgateway-route-edge.yaml)"
echo

echo "Make the Istio Ingress Gateway route target the oauth2-proxy container:"
cat oauth2-proxy/04_patch-istio-ingressgateway-route-oauth.yaml
echo
echo "Press enter to continue:"
read
oc patch route istio-ingressgateway -n $CONTROL_PLANE --patch "$(cat oauth2-proxy/04_patch-istio-ingressgateway-route-oauth.yaml)" 
echo

restore_oauth_files

echo "Retrieve the CA certificate from secret in openshift-ingress-operator project"
oc extract secret/router-ca -n openshift-ingress-operator --to=/tmp/ --confirm

echo "Create a secret from the CA certificate in control-plane project"
oc create secret generic openshift-wildcard --from-file=extra.pem=/tmp/tls.crt -n $CONTROL_PLANE

echo "Mount the CA secret at the specific location /cacerts/extra.pem in istiod pod"
oc set volumes -n $CONTROL_PLANE deployment/istiod-basic-install --add  --name=extracacerts  --mount-path=/cacerts  --secret-name=openshift-wildcard  --containers=discovery

echo "Cleaning up extracted certs"
rm -f /tmp/tls.key /tmp/tls.cert

RHSSO_ROUTE=$(oc get route keycloak -n $RHSSO | grep rhsso | awk '{print $2}')

echo "Validate certificate install.. perform the following in the shell prompt:"

cat << EOF
$ oc project control-plane

$ oc get pod
[...]
istiod-basic-6f5bfd89bf-v49vf          1/1     Running   0          14s
[...]

# RSH to istiod-basic pod
$ oc rsh istiod-basic-6f5bfd89bf-v49vf 

# Check connection to RHSSO without the CA
[pod] sh-4.4$ curl -I https://$RHSSO_ROUTE/auth/
curl: (60) SSL certificate problem: self signed certificate in certificate chain

# Check connection to RHSSO with the CA
[pod] sh-4.4$ curl --cacert /cacerts/extra.pem -I https://$RHSSO_ROUTE/auth/
HTTP/1.1 200 OK
EOF

