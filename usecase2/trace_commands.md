
# revoke user authorization
```
oc edit authorizationpolicy authpolicy -n bookinfo
```
# Grab all activity by user
```
oc -n control-plane logs istio-ingressgateway-{pod} oauth2-proxy | grep ${userid}
```
# Confirm Access Denied:
```
oc -n rhsso logs keycloak-0 | grep WARN
```
