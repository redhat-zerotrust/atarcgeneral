
# Work in Progress!
# Update this! - MAS

# OAuth integration with OpenShift Service Mesh - OTP and mTLS

![banner](images/banner.jpg)
<!-- TOC -->

- [Approach 3: Combining JWT-based authorization and OIDC workflow](#mutual-tls-ingress-gateway-with-openshift-service-mesh)
  - [Prerequisites](#prerequisites)
  - [Step by Step setup](#step-by-step-setup)
    - [Run mTLS Demo setup.sh from [usecase1](../usecase1) folder](#setup-control-plane-data-plane-and-deploy-demo-application)
    - [Install Bookinfo application and add to control-plane Service Mesh Member Roll](#install-bookinfo-application-and-add-to-control-plane-service-mesh-member-roll)
    - [Install and configure RHSSO](#install-and-configure-rhsso)
    - [Inject and configure OAUTH container into ingressgateway](#inject-and-configure-oauth-container-into-ingressgateway)
    - [Add AuthZ filters for JWT's](#add-authz-filters-for-jwts)
    - [Validate Realm, Client, and Authentication settings](#validate-realm-client-and-authentication-settings)
    - [Configure and enforce OTP](#-configure-and-enforce-otp)
    - [Configure Gitlab integration](#-configure-gitlab-integration)
  - [Interactive Command Line setup](#interactive-command-line-setup)

<!-- /TOC -->

## Background 

This demonstration draws directly from a Red Hat blog https://cloud.redhat.com/blog/restricting-user-access-via-service-mesh-2.0-and-red-hat-single-sign-on by Gaetan Hurel and its accompanying source material which heavily referenced throughout this example. In particular, approach 3 best embodies a Zero-Trust Authentication and Authorization workflow. We will use this approach, built upon mTLS enabled infrastructure s enabled in scenario 1, and demonstrate the integration of Multi-Factor Authentication.  


## Approach 3: Combining JWT-based authorization and OIDC workflow

This approach combines the use of `RequestAuthentication` and `AuthorizationPolicy` objects as done for approach 1, and the injection of the oauth2-proxy container as done in the approach 2. In this approach, the oauth2-proxy container extracts the JWT token from the authentication cookie, and forwards it to the istio-proxy container alongside the HTTP request (using the `X-Forwarded-Access-Token` HTTP header). As a result, an automated OIDC workflow to authenticate the user is performed, and can be, if needed, combined to a fine-grained authorization based on JWT token fields (e.g. simple auth for 'non-secure' apps, auth + JWT field for more secure apps).

![](images/approach_3_best_of_both.png)

The workflow is as follows:

1. the user performs an unauthenticated HTTP request to `https://<route>/productpage`;
2. the oauth2-proxy inside the Istio ingress gateway pod initiates the OIDC workflow to authenticate the user; user authenticates to RHSSO (not shown on the picture);
3. the user performs an authenticated HTTP request to `https://<route>/productpage`; the authentication is checked by the oauth2-proxy using HTTP cookies;
4. the oauth2-proxy extracts the JWT token from the authentication cookie and forwards it locally (same pod) alongside the HTTP request to the istio-proxy container of the Istio ingress gateway;
5. the istio-proxy container of the Istio ingress gateway forwards the request and the JWT token to the istio-proxy container of the productpage pod;
6. the istio-proxy container of the productpage pod checks the validity of the JWT token depending on the `RequestAuthentication` and `AuthorizationPolicy` objects deployed beforehand;
7. if the JWT token is valid, user accesses `/productpage` - otherwise, an error message is returned to the user (code 404, message "RBAC denied" or others).


## Prerequisites

Prerequisites are install Operators required by OpenShift Service Mesh. You need to install following Operators from OperatorHub.

  - ElasticSearch
  - Jaeger
  - Kiali
  - OpenShift Service Mesh
  - RHSSO 
  - mTLS demo setup from [usecase1](../usecase1)

## Step by Step setup

### Run mTLS Demo setup.sh from [usecase1](../usecase1) folder

[setup.sh](setup.sh) will automate create control plane, data plane, deploy applications and configured mTLS for all communications including ingress. 

```
$ cd ../usecase1
$ ./setup.sh
$ cd ../usecase2
```

### Install Bookinfo application and add to control-plane Service Mesh Member Roll
  
### Install and configure RHSSO

### Inject and configure OAUTH container into ingressgateway

### Add AuthZ filters for JWT's 

### Validate Realm, Client, and Authentication settings

![](images/realm_login.png)

![](images/clients_istio_settings.png)

![](images/authentication_flows.png)

#### - Configure and enforce OTP

![](images/authentication_flows.png)

![](images/authentication_required_actions.png)

#### - Configure Gitlab integration

![](images/identity_providers_gitlab.png)

![](images/rhsso_config_in_gitlab.png)

  
## Interactive Command Line setup

Demo assumes the following values:
 - Service Mesh Control Plane: "control-plane"
 - Bookinfo Project: "bookinfo"
 - RHSSO Project: "rhsso"

[01_setup_bookinfo.sh](01_setup_bookinfo.sh) will create bookinfo project, deploy applications, and configure for mTLS environment.

[02_setup_rhsso_config.sh](02_setup_rhsso_config.sh) will configure RHSSO with Realm, Clients, Roles, and a basic user

[03_setup_oauth2-proxy.sh](03_setup_oauth2-proxy.sh) will inject and configure Oauth2 container into istio ingressgateway

[04_setup_jwt.sh](04_setup_jwt.sh) will create authenticaiton and authorization rules


