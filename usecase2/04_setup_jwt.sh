#!/bin/bash

function backup_jwt_files(){
    for file in $(ls jwt/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_jwt_files(){
    for file in $(ls jwt/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f jwt/*.bak
}

SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Route will under domain *.${DOMAIN}"

echo "Do you want to change domain? (y/n): "
read CHANGE_DOMAIN
echo

if [ "$CHANGE_DOMAIN" = "Y" ] || [ "$CHANGE_DOMAIN" = "y" ];
then
    echo "Enter new domain name: "
    read DOMAIN
fi

RHSSO="rhsso"
echo "JWT Setup will proceed under RHSSO $RHSSO"

echo "Do you want to change RHSSO project? (y/n): "
read CHANGE_RHSSO
echo

if [ "$CHANGE_RHSSO" = "Y" ] || [ "$CHANGE_RHSSO" = "y" ];
then
    echo "Enter new RHSSO project name: "
    read RHSSO
fi


BOOKINFO="bookinfo"
echo "JWT Setup will proceed under project $BOOKINFO"

echo "Do you want to change project? (y/n): "
read CHANGE_PROJECT
echo

if [ "$CHANGE_PROJECT" = "Y" ] || [ "$CHANGE_PROJECT" = "y" ];
then
    echo "Enter new project name: "
    read BOOKINFO
fi

backup_jwt_files

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/RHSSO/'"$RHSSO"'/' jwt/*.yaml
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' jwt/*.yaml
  sed -i '' 's/BOOKINFO/'"$BOOKINFO"'/' jwt/*.yaml
else
  sed -i 's/RHSSO/'"$RHSSO"'/' jwt/*.yaml
  sed -i 's/DOMAIN/'"$DOMAIN"'/' jwt/*.yaml
  sed -i 's/BOOKINFO/'"$BOOKINFO"'/' jwt/*.yaml
fi

echo "Set up request authentication policies:"
cat jwt/01_requestauthentication.yaml
echo
echo "Press enter to continue:"
read
oc apply -f jwt/01_requestauthentication.yaml -n $BOOKINFO
echo

echo "Set up authorization policies:"
cat jwt/02_authpolicy_allow_from_servicemesh-lab_realm.yaml
echo
echo "Press enter to continue:"
read
oc apply -f jwt/02_authpolicy_allow_from_servicemesh-lab_realm.yaml -n $BOOKINFO

restore_jwt_files

echo "Command line portions of the lab setup are complete"
echo
echo "Please verify settings and continue with setup."

if [ -f demo_notes ] ; then
    cat demo_notes
fi


