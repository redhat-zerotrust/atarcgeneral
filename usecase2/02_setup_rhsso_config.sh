#!/bin/bash

function check_pod(){
    PROJECT=$1
    NUM=$2
    CONDITION=$3
    COUNT=0
    while [ ${COUNT} -lt ${NUM} ];
    do
        clear
        oc get pods -n ${PROJECT}
        sleep 5
        COUNT=$( oc get pods -n ${PROJECT} --field-selector=status.phase=Running --no-headers|wc -l)
    done
}

function backup_rhsso_files(){
    for file in $(ls rhsso/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_rhsso_files(){
    for file in $(ls rhsso/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f rhsso/*.bak
}

SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Redirect Route will under domain *.${DOMAIN}"


PLATFORM=$(uname)

if ! hash oc 2>/dev/null
then
    echo "'oc' was not found in PATH"
    echo "Download from https://mirror.openshift.com/pub/openshift-v4/clients/oc/"
    exit
fi

if ! hash oc whoami 2>/dev/null
then
    echo "You need to login to your cluster with oc login --server=<URL to API>"
    exit
fi

echo "Enter the name of RHSSO project: "
read RHSSO
echo

echo "Enter the name of Service Mesh Control Plane project: "
read CONTROL_PLANE
echo

backup_rhsso_files

oc get project $RHSSO > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $RHSSO exists, continuing..."
  sleep 2
else
  echo "Create project: $RHSSO and install RHSSO Operator"
  exit 1
fi

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' rhsso/*.yaml
else
  sed -i 's/DOMAIN/'"$DOMAIN"'/' rhsso/*.yaml
fi

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/RHSSO/'"$RHSSO"'/' rhsso/*.yaml
else
  sed -i 's/RHSSO/'"$RHSSO"'/' rhsso/*.yaml
fi

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' rhsso/*.yaml
else
  sed -i 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' rhsso/*.yaml
fi

echo "Create the RHSSO deployment as follows:"
cat rhsso/01_rhsso.yaml
echo
echo "Hit enter to continue:"
read
oc apply -f rhsso/01_rhsso.yaml -n $RHSSO

echo 
echo "Wait for rhsso startup..."
check_pod $RHSSO 3 Running
echo

echo "Create the RHSSO servicemesh-lab realm:"
cat rhsso/02_servicemesh-realm.yaml
echo
echo "Hit enter to continue:"
read
oc apply -f rhsso/02_servicemesh-realm.yaml -n $RHSSO

echo
echo "Create a client istio inside the realm servicemesh-lab."
echo "Note: the field secret is set randomly and you can leave as it is (we will use "
echo "this secret later); even if the route matching the redirectUris is currently"
echo "using http (the route of the Istio default ingress gateway), leave https in "
echo "the redirectUris field (the route will be patched later)"
cat rhsso/03_istio-client.yaml
echo
echo "Hit enter to continue:"
read
oc apply -f rhsso/03_istio-client.yaml -n $RHSSO

echo
echo "Create the local user localuser:localuser inside the realm servicemesh-lab"
cat rhsso/04_local-user.yaml
echo
echo "Hit enter to continue:"
oc apply -f rhsso/04_local-user.yaml -n $RHSSO

restore_rhsso_files

# Sleep a bit
#sleep 30

RHSSO_ROUTE=$(oc get route keycloak -n $RHSSO | grep keycloak | awk '{print $2}')
RHSSO_ADMIN_CREDENTIAL=$(oc get secret credential-rhsso-simple --template={{.data.ADMIN_PASSWORD}} -n $RHSSO | base64 --decode)

echo
echo "Log into https://${RHSSO_ROUTE}/auth as admin with credential ${RHSSO_ADMIN_CREDENTIAL} and check the following:"
echo
echo " - ensure Standard Flow Enabled is enabled for this client; Clients>Istio>Settings>Line 10"
echo " - ensure that the Access Type value is set to confidential; if not, set it Clients>Istio>Settings>Line 9"
echo " - click on the button Save at the bottom of the page; a new "Credentials" tab has appeared at the top of the page,"
echo "and you can check in this tab that the client secret is matching the value set in 03_istio-client.yaml (if not, simply use the new value)."
echo " - On Client Scopes tab, verify Assigned Default Client Scopes contains email and profile"
echo
echo "You can test to login as localuser:localuser by opening https://$RHSSO_ROUTE/auth/realms/servicemesh-lab/account in a browser."
echo
echo "Retaining URLs in demo_notes..."

cat << EOF >> demo_notes

https://${RHSSO_ROUTE}/auth credential ${RHSSO_ADMIN_CREDENTIAL}
https://$RHSSO_ROUTE/auth/realms/servicemesh-lab/account

EOF

