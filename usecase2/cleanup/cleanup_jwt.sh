#!/bin/bash

BOOKINFO="bookinfo"
echo "JWT Cleanup will proceed under project $BOOKINFO"

echo "Do you want to change project? (y/n): "
read CHANGE_PROJECT
echo

if [ "$CHANGE_PROJECT" = "Y" ] || [ "$CHANGE_PROJECT" = "y" ];
then
    echo "Enter new project name: "
    read BOOKINFO
fi

oc delete -f jwt/02_authpolicy_allow_from_servicemesh-lab_realm.yaml -n $BOOKINFO
oc delete -f jwt/01_requestauthentication.yaml -n $BOOKINFO

echo "Cleanup for injected Oauth2 will occur during MTLS cleanup"


