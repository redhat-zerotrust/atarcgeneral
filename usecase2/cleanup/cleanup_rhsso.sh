#!/bin/bash
echo "Enter the name of RHSSO project: "
read RHSSO
echo

oc delete -f rhsso/04_local-user.yaml -n $RHSSO
oc delete -f rhsso/03_istio-client.yaml -n $RHSSO
oc delete -f rhsso/02_servicemesh-realm.yaml -n $RHSSO
oc delete -f rhsso/01_rhsso.yaml -n $RHSSO

