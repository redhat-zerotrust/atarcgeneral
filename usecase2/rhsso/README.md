
## KeyCloak
  - Realm->Login:

    - User Registration: On
    - Login with email: On

  - Clients -> Istio -> Settings
    - Enabled: on
    - Client ProtocolL openid-connect
    - Access Type: confidential
    - Standard Flow Enabled: on
    - Direct Access Grants Enmabled: on

   - Clients -> Istio -> Client Scopes
     - Default Client Scopes (assigned):
       - email
       - profile

   - Identify Providers
     - Gitlab
       - Application ID and Secret from Gitlab
       - Default Scopes: api read_user openid profile email
       - Store Tokens: on
       - Store Tokens Readable: on
       - Enabled: on
       - Trust email: on
       - First Login Flow: first broker login
       - sync mode: force

   - Authentication:
     - Flows: Http challenge
       - Basic Auth Password+OTP: required
     - Required Actions:
       - Configure OTP: Default Action
 
## GitLab
  - User Settings -> Applications
    - Callback URL from RHSSO
    - confidential: yes
    - Scopes: api read_user openid profile email
