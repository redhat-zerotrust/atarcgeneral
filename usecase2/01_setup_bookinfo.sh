#!/bin/bash
function check_pod(){
    PROJECT=$1
    NUM=$2
    CONDITION=$3
    COUNT=0
    while [ ${COUNT} -lt ${NUM} ];
    do
        clear
        oc get pods -n ${PROJECT}
        sleep 5
        COUNT=$( oc get pods -n ${PROJECT} --field-selector=status.phase=Running --no-headers|wc -l)
    done
}

function backup_istio_files(){
    for file in $(ls istio/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_istio_files(){
    for file in $(ls istio/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f istio/*.bak
}

function backup_bookinfo_files(){
    for file in $(ls bookinfo/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_bookinfo_files(){
    for file in $(ls bookinfo/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f bookinfo/*.bak
}

function get_control_plane_status(){
  DONE=1
  while [ $DONE -ne 0 ];
  do
    clear
    CURRENT_STATUS=$(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.annotations.readyComponentCount}')
    printf "Ready Component Count: %s\n" "$CURRENT_STATUS"
    READY=$(echo $CURRENT_STATUS|awk -F'/' '{print $1}')
    TOTAL=$(echo $CURRENT_STATUS|awk -F'/' '{print $2}')
    if [ $READY -gt 0 ];
    then
      printf "Ready: \n"
      for i in $(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.readiness.components.ready[*]}')
      do
        printf "=> %s\n" "$i"
      done
    fi
    if [ $READY -eq  $TOTAL ];
    then
      DONE=0
    fi
    sleep 20
  done
}

function verify_sidecar(){
  PROJECT=$1
  for pod in $(oc get pods -n $PROJECT --no-headers -o=custom-columns='DATA:metadata.name')
  do
    NUM=$(oc get pod $pod -n $PROJECT -o jsonpath='{.spec.containers[*].name}' | wc -w)
    if [ $NUM -lt 2 ];
    then
      echo "Sidecar not found for pod $pod"
      oc delete pod $pod -n $PROJECT
    else
      echo "pod $pod already has 2 containers"
    fi
  done
}

PLATFORM=$(uname)

if ! hash oc 2>/dev/null
then
    echo "'oc' was not found in PATH"
    echo "Download from https://mirror.openshift.com/pub/openshift-v4/clients/oc/"
    exit
fi

if ! hash oc whoami 2>/dev/null
then
    echo "You need to login to your cluster with oc login --server=<URL to API>"
    exit 
fi

echo "Enter the name of Service Mesh Control Plane project: "
read CONTROL_PLANE
echo

oc get project $CONTROL_PLANE > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $CONTROL_PLANE already exists"
  sleep 2
else
  echo "Creating project: $CONTROL_PLANE"
  oc new-project $CONTROL_PLANE --display-name $CONTROL_PLANE --description="Service Mesh Control Plane" 1>/dev/null
fi

echo "Enter the name of Bookinfo project: "
read BOOKINFO
echo

oc get project $BOOKINFO > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $BOOKINFO already exists"
  sleep 2
else
  echo "Creating project: $BOOKINFO"
  oc new-project $BOOKINFO --display-name $BOOKINFO --description="Bookinfo Demo" 1>/dev/null
fi

SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Route will under domain *.${DOMAIN}"

echo "Do you want to change domain? (y/n): "
read CHANGE_DOMAIN
echo

if [ "$CHANGE_DOMAIN" = "Y" ] || [ "$CHANGE_DOMAIN" = "y" ];
then
    echo "Enter new domain name: "
    read DOMAIN
fi

echo "Check Control Plane"
get_control_plane_status

echo "Join Bookinfo to Control Plane"
echo "Add - bookinfo to spec: to save time"
echo "press enter to continue:"
read

oc -n $CONTROL_PLANE edit smmr

echo 
oc describe smmr/default -n $CONTROL_PLANE | grep -A3 Spec:
sleep 5

backup_bookinfo_files

echo "Deploy applications to $BOOKINFO"
oc apply -f bookinfo/01_bookinfo.yaml -n $BOOKINFO

echo "Create Gateway and VirtualService"
oc apply -f bookinfo/02_bookinfo-gateway.yaml -n $BOOKINFO

check_pod $BOOKINFO 6 Running

restore_bookinfo_files

backup_istio_files

if [ "$PLATFORM" = 'Darwin' ];
then
  sed -i '' 's/BOOKINFO/'"$BOOKINFO"'/' istio/*.yaml
  sed -i '' 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' istio/*.yaml
  sed -i '' 's/DOMAIN/'"$DOMAIN"'/' istio/*.yaml
else
  sed -i 's/BOOKINFO/'"$BOOKINFO"'/' istio/*.yaml
  sed -i 's/CONTROL_PLANE/'"$CONTROL_PLANE"'/' istio/*.yaml
  sed -i 's/DOMAIN/'"$DOMAIN"'/' istio/*.yaml
fi

echo 
echo "Apply istio configuration for mutual TLS authentication to applications"
oc apply -f istio/03_destination-rule-all-mtls.yaml -n $BOOKINFO
oc apply -f istio/04_details-peer-authentication.yaml -n $BOOKINFO
oc apply -f istio/05_productpage-peer-authentication.yaml -n $BOOKINFO
oc apply -f istio/06_ratings-peer-authentication.yaml -n $BOOKINFO
oc apply -f istio/07_reviews-peer-authentication.yaml -n $BOOKINFO
echo 

#echo "Create Gateway and Route"
#oc apply -f istio/gateway.yaml -n $CONTROL_PLANE
#oc apply -f istio/route.yaml -n $CONTROL_PLANE

restore_istio_files

echo
echo "Check pods in $BOOKINFO"
verify_sidecar $BOOKINFO


GATEWAY_URL=$(oc -n $CONTROL_PLANE get route istio-ingressgateway -o jsonpath='{.spec.host}')

echo "Check http://$GATEWAY_URL/productpage:"
echo
curl -I http://$GATEWAY_URL/productpage
echo
echo "Check https://$GATEWAY_URL/productpage:"
echo
curl -I https://$GATEWAY_URL/productpage
echo "Contact host to validate service in incognito window:"
echo "https://$GATEWAY_URL/productpage"

echo "Retaining URL in demo_notes..."
cat << EOF >> demo_notes
Contact host to validate service in incognito window:
https://$GATEWAY_URL/productpage

EOF

