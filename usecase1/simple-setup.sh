#!/bin/bash
function check_pod(){
    PROJECT=$1
    NUM=$2
    CONDITION=$3
    COUNT=0
    while [ ${COUNT} -lt ${NUM} ];
    do
        clear
        oc get pods -n ${PROJECT}
        sleep 5
        COUNT=$( oc get pods -n ${PROJECT} --field-selector=status.phase=Running --no-headers|wc -l)
    done
}

function backup_istio_files(){
    for file in $(ls istio/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.yaml $file_name.bak
    done
}

function restore_istio_files(){
    for file in $(ls istio/*.yaml)
    do
        file_name=$(echo $file|awk -F'.' '{print $1}')
        cp $file_name.bak $file_name.yaml
    done
    rm -f istio/*.bak
}

function get_control_plane_status(){
  DONE=1
  while [ $DONE -ne 0 ];
  do
    clear
    CURRENT_STATUS=$(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.annotations.readyComponentCount}')
    printf "Ready Component Count: %s\n" "$CURRENT_STATUS"
    READY=$(echo $CURRENT_STATUS|awk -F'/' '{print $1}')
    TOTAL=$(echo $CURRENT_STATUS|awk -F'/' '{print $2}')
    if [ $READY -gt 0 ];
    then
      printf "Ready: \n"
      for i in $(oc get smcp basic-install -n $CONTROL_PLANE -o jsonpath='{.status.readiness.components.ready[*]}')
      do
        printf "=> %s\n" "$i"
      done
    fi
    if [ $READY -eq  $TOTAL ];
    then
      DONE=0
    fi
    sleep 20
  done
}

function verify_sidecar(){
  PROJECT=$1
  for pod in $(oc get pods -n $PROJECT --no-headers -o=custom-columns='DATA:metadata.name')
  do
    NUM=$(oc get pod $pod -n $PROJECT -o jsonpath='{.spec.containers[*].name}' | wc -w)
    if [ $NUM -lt 2 ];
    then
      echo "Sidecar not found for pod $pod"
      oc delete pod $pod -n $PROJECT
    else
      echo "pod $pod already has 2 containers"
    fi
  done
}

PLATFORM=$(uname)

if ! hash oc 2>/dev/null
then
    echo "'oc' was not found in PATH"
    echo "Download from https://mirror.openshift.com/pub/openshift-v4/clients/oc/"
    exit
fi

if ! hash oc whoami 2>/dev/null
then
    echo "You need to login to your cluster with oc login --server=<URL to API>"
    exit 
fi

echo "Enter the name of Service Mesh Control Plane project: "
read CONTROL_PLANE
echo

oc get project $CONTROL_PLANE > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $CONTROL_PLANE already exists"
  sleep 2
else
  echo "Creating project: $CONTROL_PLANE"
  oc new-project $CONTROL_PLANE --display-name $CONTROL_PLANE --description="Service Mesh Control Plane" 1>/dev/null
fi

echo "Enter the name of Service Mesh Data Plane project: "
read DATA_PLANE
echo

oc get project $DATA_PLANE > /dev/null 2>&1
if [ $? -eq 0 ]
then
  echo "Project $DATA_PLANE already exists"
  sleep 2
else
  echo "Creating project: $DATA_PLANE"
  oc new-project $DATA_PLANE --display-name $DATA_PLANE --description="Service Mesh Data Plane" 1>/dev/null
fi

SUBDOMAIN=$(oc whoami --show-console  | awk -F'apps.' '{print $2}')
DOMAIN="apps.${SUBDOMAIN}"
echo "Route will under domain *.${DOMAIN}"

echo "Do you want to change domain? (y/n): "
read CHANGE_DOMAIN
echo

if [ "$CHANGE_DOMAIN" = "Y" ] || [ "$CHANGE_DOMAIN" = "y" ];
then
    echo "Enter new domain name: "
    read DOMAIN
fi

echo "Create Control Plane"
echo "Script will automatically continue to next steps when control plane is finished"
oc create -f https://gitlab.com/redhat-zerotrust/openshift-service-mesh-ingress-mtls/-/raw/main/setup-ossm/smcp.yaml -n $CONTROL_PLANE
#oc apply -f setup-ossm/smcp.yaml -n $CONTROL_PLANE
echo "Wait 40 sec before check control plane status"
sleep 40
get_control_plane_status

echo "Join Data Plane to Control Plane"
#Join data-plane namespace into control-plane
oc create -f https://gitlab.com/redhat-zerotrust/openshift-service-mesh-istio-gateway/-/raw/main/member-roll.yaml -n $CONTROL_PLANE
sleep 5
#You can check status by
oc describe smmr default -n control-plane

oc create -f https://gitlab.com/redhat-zerotrust/openshift-service-mesh-istio-gateway/-/raw/main/app.yaml -n data-plane

echo "Annotate the sidecards"
#Inject sidecars to frontend and backend deployment
oc patch deployment/frontend-v1 -p '{"spec":{"template":{"metadata":{"annotations":{"sidecar.istio.io/inject":"true"}}}}}' -n $DATA_PLANE
oc patch deployment/backend-v1 -p '{"spec":{"template":{"metadata":{"annotations":{"sidecar.istio.io/inject":"true"}}}}}' -n $DATA_PLANE
sleep 40

echo "Check pods"
oc get pods -n $DATA_PLANE

echo "Check API" 
curl -s -w"\n Response Code:%{http_code}\n"  $(oc get route frontend -n $DATA_PLANE -o jsonpath='{.spec.host}')

echo "make available"
#Patch deployment
oc patch deployment/frontend-v1 -p '{"spec":{"template":{"metadata":{"labels":{"maistra.io/expose-route":"true"}}}}}' -n $DATA_PLANE
echo "test with curl"
#Test with cURL again
curl $(oc get route frontend -n $DATA_PLANE -o jsonpath='{.spec.host}')
